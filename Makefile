CC = gcc
INCDIR = header
SRCDIR = src
OBJDIR = obj
CFLAGS = -c -Wall -lwiringPi -I$(INCDIR)
LDFLAGS = -lwiringPi -lncurses -lpthread
SRC = $(wildcard $(SRCDIR)/*.c)
OBJ = $(patsubst $(SRCDIR)/%.c, $(OBJDIR)/%.o, $(SRC))
EXE = ./p1.out

all: clean $(EXE)

$(EXE): $(OBJ)
				$(CC) $(LDFLAGS) $(OBJDIR)/*.o -o $@

$(OBJDIR)/%.o : $(SRCDIR)/%.c
							 	@mkdir -p $(@D)
							 	$(CC) $(CFLAGS) $< -o $@

clean:
			-rm -f $(OBJDIR)/*.o $(EXE)
