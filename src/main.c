#include<fcntl.h>          //Used for UART
#include<termios.h>        //Used for UART
#include<wiringPiI2C.h>
#include<linux/i2c-dev.h>
#include<sys/ioctl.h>
#include<signal.h>
#include<pthread.h>
#include<time.h>

//#include<bcm2835.h>
#include"../header/crc.h"
#include"../header/uart.h"
#include"../header/pid.h"
#include"../header/lcd_driver.h"
#include"../header/i2c.h"
#include"../header/data.h"
#include"../header/gpio.h"
#include"../header/menu.h"

int entrada_padrao;

float uart(unsigned char code);
void init_sys();

void init_sys(){
  if(wiringPiSetup() == -1)
    exit(1);

  fd = wiringPiI2CSetup(BME280_ADD);
  
  lcd_init();
  init_gpio();
  ClrLcd();

  dados_temp.sinal_controle_MAX = 100;
  dados_temp.sinal_controle_MIN = -100;

  entrada_padrao = -1;

  pid_inicializa_const(5, 1, 5);
  erro_total = 0;
  erro_anterior = 0;
  T = 1;
}

void update_refs(){
  init_bme();
  if(entrada_padrao == -1 || entrada_padrao < dados_temp.temp_amb || entrada_padrao > 100)
    dados_temp.temp_ref = uart(TEMP_POT);
  else
    dados_temp.temp_ref = entrada_padrao;
  dados_temp.temp_int = uart(TEMP_INT);
  pid_atualiza_ref(dados_temp.temp_ref);
}

void write_lcd(){
  char * str = malloc(20*sizeof(char));

  sprintf(str, "TI:%.1f TR:%.1f", dados_temp.temp_int, (float) dados_temp.temp_ref);
  lcdLoc(LINE1);
  typeln(str);
  
  sprintf(str, "TA:%.1f", dados_temp.temp_amb);
  lcdLoc(LINE2);
  typeln(str);

  free(str);
}

void write_csv(int res){
  FILE * fp = fopen("out.csv", "a");
  time_t rawtime;
  struct tm * timeinfo;

  time(&rawtime);
  timeinfo = localtime(&rawtime);

  fprintf(fp, "%s;%d;%d;%.2f;%.2f;\n", asctime(timeinfo), res, dados_temp.temp_ref, dados_temp.temp_int, dados_temp.temp_amb);

  fclose(fp);
}

void run_sys(){
  while(1){
    update_refs();

    write_lcd();
    
    int res = (int) pid_controle(dados_temp.temp_int);
//    printf("-----------------------------------------------\n");
//    printf("%d;%d;%.2f;%.2f;\n", res, dados_temp.temp_ref, dados_temp.temp_int, dados_temp.temp_amb);
    write_csv(res);

    if(res > 0){
//      printf("Ligando RESISTOR\n");
      aciona_gpio(RESI_ADDR, res);
      usleep(DELAY_GPIO);
      aciona_gpio(VENT_ADDR, 0);
      usleep(DELAY_GPIO);
    }else if(res <= 0 && res > -40){
//      printf("DESLIGANDO\n");
      aciona_gpio(RESI_ADDR, 0);
      usleep(DELAY_GPIO);
      aciona_gpio(VENT_ADDR, 0);
      usleep(DELAY_GPIO);
    }else{
//      printf("Ligando VENTOINHA\n");
      aciona_gpio(VENT_ADDR, -1 * res);
      usleep(DELAY_GPIO);
      aciona_gpio(RESI_ADDR, 0);
      usleep(DELAY_GPIO);
    }
    usleep(500000);
  }
}

void * ler_entrada(){
  while(1)
    scanf("%d", &entrada_padrao);
}

void * menu_func(){
  pthread_t entrada_usuario;
  pthread_create(&entrada_usuario, NULL, ler_entrada, NULL);

  int row, col;
  getmaxyx(stdscr, row, col);

  while(1){
    draw_menu();
    move(row / 2, (5*col/8));
    insert_infos(dados_temp.temp_ref, dados_temp.temp_amb, dados_temp.temp_int);
    refresh();
    usleep(500000);
  }
  
}

void end_sys(){
  end_gpio();
  endwin();
  exit(0);
}

int main(){
  signal(SIGINT, end_sys);

  FILE * fp = fopen("out.csv", "w");

  fprintf(fp, "TIME;RES;TEMP_REF;TEMP_INT;TEMP_AMB;\n");

  fclose(fp);
  
  init_sys();
  initscr();
  
  pthread_t menu;
  pthread_create(&menu, NULL, menu_func, NULL);

  run_sys();


  end_sys();

  return 0;
}

float uart(unsigned char code){
  int uart0_filestream = -1;

  inicializa_uart(&uart0_filestream);

  if(uart0_filestream == -1)exit(1);

  struct termios options;

  inicializa_opcoes(&options, uart0_filestream);
  
  envia_mensagem(uart0_filestream, 0x23, code);

  usleep(200000);

  float var = recebe_mensagem(uart0_filestream, code);

  if(var == -101)
    uart(code);

  close(uart0_filestream);

  return var;
}

