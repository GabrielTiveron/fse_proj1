#include"../header/menu.h"

void draw_box(int height, int width, int x, int y){
  for(int i = x; i < width; i++){
    for(int j = y; j < height; j++){
      if((i == x || i == width - 1) && (j > y && j < height - 1)){
        mvaddch(i,j,ACS_HLINE);
        continue;
        }
      else if(i == width - 1 && j == y){
        mvaddch(i,j,ACS_LLCORNER);
        continue;
        }
      else if(i == width -1 && j == height - 1){
        mvaddch(i,j,ACS_LRCORNER);
        continue;
        }
      else if(i == x && j == y){ // Esq sup
        mvaddch(i,j,ACS_ULCORNER);
        continue;
        }
      else if(i == x && j == height -1){
        mvaddch(i,j,ACS_URCORNER);
        continue;
        }
      else if((i > x && i < width-1) && (j == y || j == height - 1)){
        mvaddch(i,j,ACS_VLINE);
        continue;
        }
        }
    }
}

void draw_menu(){
  int row, col;
  getmaxyx(stdscr, row, col);
  draw_box(17, 15, 15, 20);
  refresh();
  draw_box(col - col/4, row - row/4, row/4, col/4);
  draw_box(col - col/4, row - row/4, row/4, col/2);
  mvprintw(row / 2 - 4, (23*col/32 - 37), "Digite uma temperatura de referencia");
  mvprintw(row / 2 - 3, (23*col/32 - 37), "Caso queira o Potenciometro digite(-1)");
  mvprintw(row / 2 + 1, (col - 3*col/5 - 14), "TEMP_AMB: ");
  mvprintw(row / 2, (col - 3*col/5 - 14), "TEMP_INT: ");
  mvprintw(row / 2 - 1, (col - 3*col/5 - 14), "TEMP_REF: ");
}

void insert_infos(float ref_, float amb_, float int_){
  int row, col;
  getmaxyx(stdscr, row, col);
  mvprintw(row / 2 + 1, (col - 3*col/5 - 3), "%.2f", amb_);
  mvprintw(row / 2 , (col - 3*col/5 - 3), "%.2f", int_);
  mvprintw(row / 2 - 1, (col - 3*col/5 - 3), "%.2f", ref_);
}
