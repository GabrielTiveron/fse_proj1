#include"../header/uart.h"

void inicializa_uart(int * uart0_filestream){
  
  //Open in non blocking read/write mode
  *uart0_filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);
  
  if(*uart0_filestream == -1){
    printf("Erro - Não foi possível iniciar a UART.\n");
  }
  else{
    //printf("UART inicializada!\n");
  }    

}

void inicializa_opcoes(struct termios * options, int uart0_filestream){

  tcgetattr(uart0_filestream, options);
  options->c_cflag = B9600 | CS8 | CLOCAL | CREAD;     //<Set baud rate
  options->c_iflag = IGNPAR;
  options->c_oflag = 0;
  options->c_lflag = 0;
  tcflush(uart0_filestream, TCIFLUSH);
  tcsetattr(uart0_filestream, TCSANOW, options);

}

void envia_mensagem(int uart0_filestream, unsigned char cod, unsigned char sub_cod){
//  unsigned char tx_buffer[20];
  unsigned char new_buffer[20], *p_new_buffer;
  //unsigned char *p_tx_buffer;
  
  p_new_buffer = &new_buffer[0];
  *p_new_buffer++ = 0x01;
  *p_new_buffer++ = cod;
  *p_new_buffer++ = sub_cod;
  *p_new_buffer++ = 3;
  *p_new_buffer++ = 4;
  *p_new_buffer++ = 7;
  *p_new_buffer++ = 1;
  short answ = calcula_CRC(new_buffer, 7);
  *p_new_buffer++ = answ & 0xff;
  *p_new_buffer++ = (answ >> 8) & 0xff;

  //printf("Buffers de memória criados.\n");

  if(uart0_filestream != -1){
//    printf("Enviando para UART . . .");
    int count = write(uart0_filestream, &new_buffer[0], (p_new_buffer - &new_buffer[0]));
    if(count < 0){
      printf("UART TX error\n");
    }
    else{
  //    printf("escrito.\n");
    }
  }
}

float recebe_mensagem(int uart0_filestream, unsigned char code){
  float value;
  
  if(uart0_filestream != -1){
    // Read up to 255 characters from the port if they are there
    unsigned char rx_buffer[256];

    int rx_length = read(uart0_filestream, (void*)rx_buffer, 255);      //Filestream, buffer to store in, number of bytes to read (max)
    if(rx_length < 0){
      printf("Erro na leitura.\n"); //An error occured (will occur if there are no bytes)
    }
    else if (rx_length == 0){
      printf("Nenhum dado disponível.\n"); //No data waiting
    }
    else{
      unsigned char  ptr[20];
      //Bytes received
      rx_buffer[rx_length] = '\0';
      memcpy(&value, &rx_buffer[3], 4);
      short answ = calcula_CRC(rx_buffer, 7);
      if((answ & 0xff) == rx_buffer[7] && ((answ >> 8) & 0xff) == rx_buffer[8])
        return value;
      else
        return -101;
    }
  }
  return -101;
}
