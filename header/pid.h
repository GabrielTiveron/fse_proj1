#ifndef PID_H
#define PID_H

#include"./i2c.h"

#define TEMP_MAX 50
#define TEMP_MIN 20

double temp_ref;
double Kp, Ki, Kd;
double saida_medida, sinal_controle;
int T;
unsigned long last_time;
double erro_total, erro_anterior;
double temp_referencia;


void pid_inicializa_const(double,double,double);
void pid_atualiza_ref(double);
double pid_controle(double);


#endif
