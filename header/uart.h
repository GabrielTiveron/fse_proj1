#ifndef COMUNICATION_H
#define COMUNICATION_H

#include"../header/crc.h"
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<fcntl.h>          //Used for UART
#include<termios.h>        //Used for UART

#define TEMP_INT 0xc1
#define TEMP_POT 0xc2

//const int MATRICULA[4] = {3, 4, 7, 1};

void inicializa_uart(int*);
void inicializa_opcoes(struct termios*, int);
void envia_mensagem(int,unsigned char, unsigned char);
float recebe_mensagem(int, unsigned char);


#endif
