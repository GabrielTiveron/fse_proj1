# Projeto 1 - FSE 2020.2
### Resumo
O projeto consiste em gerenciar dois atuadores, um resistor e uma ventoinha, para que a temperatura do sistema atinja uma certa temperatura de referência.

### Como executar
Tendo todas as dependencias instaladas : wiringPi, wiringPiI2C; basta executar o comando ```make``` para gerar um arquivo executável. Para executar o arquivo é necessário o comando ```./p1.out```.

### O programa

Durante a execução é apresentado ao usuário apenas um menu onde pode-se observar as temperaturas de interesse: interna (TI), ambiente (TA) e de referência (TR). Ao digitar um número na entrada padrão éé possível alterar a temperatura de referência e, caso a entrada esteja fora do domínio ```TA < x < 100```. Para finalizar a execução do programa basta enviar um sinal ```SIGINT```, dessa forma o programa irá desligar os atuadores e as comunicações e encerrar a execução.

### Resultados
Os dados apresentados abaixo foram extraídos da execução do programa durante 10 minutos e apresentaram uma boa consistência.

|Variação da temperatura | Controle dos atuadores|
|:----------------------:|:----------------------:|
|![Img1](/img/Plot_temp.png)| ![Img2](/img/Plot_cont.png)|

A porcentagem apresentada é interpretada da seguinte forma: no alcance de 0 a 100 positivo, é relativo a ativação do resistor, já de 0 a -100, é relativo a ativação da ventoinha.

### Problemas conhecidos

* Ao digitar um número no menu é necessário pressionar enter para que o número entre no sistema, porém enquanto digita o número não há como visualizá-lo. 
* Ao ter erro de comunicação, a aplicação tenta refazer a operação, porém não para de tentar refazer a reconexão enquanto nãão recebe uma resposta válida.

### Referência

[Criação do gráfico](https://chart-studio.plotly.com/create/#/)

[Menu da aplicação](https://tldp.org/HOWTO/NCURSES-Programming-HOWTO/)

[Enunciado do Projeto](https://gitlab.com/fse_fga/projetos_2020_2/projeto-1-2020.2)
