import pandas as pd


dados = pd.read_csv("out.csv", delimiter=';')

dados['TIME_'] = pd.to_datetime(dados['TIME'])
dados['TIME_'] = [x.strftime('%H:%M:%S') for x in dados['TIME_']]

dados['VENT'] = [x * -1 if x < -39 else 0 for x in dados['RES']]
dados['RESIS'] = [x if x > 0 else 0 for x in dados['RES']]

print(dados.dtypes)
print(dados.head())
dados.to_csv("Teste.csv")
